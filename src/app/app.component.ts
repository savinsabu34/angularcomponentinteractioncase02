import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'componentInteractionCase02';
  parent_rectangle: string = "Parent rectangle";
  parent_triangle: string = "triangle";
  parent_circle: string = "circle";
  parent_diamond: string = "diamond";

  dumpToParent(fromChild_rect:any){
    this.parent_rectangle = fromChild_rect;
  }
}
