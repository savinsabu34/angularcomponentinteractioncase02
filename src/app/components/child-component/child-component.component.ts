 import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.scss']
})
export class ChildComponentComponent implements OnInit {

  child_rectangle: string = "Child rectangle";
  child_triangle: string = "triangle";
  child_circle: string = "circle";
  child_diamond: string = "diamond";
  @Output() child_rectangle_to_emit = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }
  sendMessageToParent(child_rectangle){
    this.child_rectangle_to_emit.emit(child_rectangle)
  }

}
